package io.getarrays.userservice.Integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import io.getarrays.userservice.model.Usuario;
import io.getarrays.userservice.repository.UserRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    private final Faker faker = new Faker();

    @BeforeAll
    static void beforeAll() {

    }

    @SneakyThrows
    @Test
    void canRegisterAnNewUser() {
        // given
        String username = "marcusvilela";

        Usuario userTest = new Usuario(
                null,
                "Marcus",
                "marcusvilela",
                "1234",
                new ArrayList<>()
        );

        var content = new JSONObject();

        content.put("id", userTest.getId());
        content.put("name", userTest.getName());
        content.put("username", userTest.getUsername());
        content.put("password", userTest.getPassword());

        log.info(content.toString());

        String tokenOwner = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9." +
                "eyJzdWIiOiJDUjciLCJyb2xlcyI6WyJST0xFX09XTkVSIl0sImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9hcGkvbG9naW4iLCJleHAiOjE2MzM5NTA3Njl9." +
                "B7XDRf-yNwOhYFKsia8UcbaVCtTwSrlG_D23B5behEA";


        // when
        ResultActions resultActions = mockMvc
                .perform(post("/api/users/save")
                        .header("Authorization", tokenOwner)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content.toString()));

        // then
        resultActions.andExpect(status().isCreated());
        Usuario students = userRepository.findByUsername(username);
        assertThat(students.getName()).isEqualTo(userTest.getName());
    }

    @SneakyThrows
    @Test
    void canDeleteAnUser() {
        // given
        String username = "Hulk";

        String name = String.format(
                "%s %s",
                faker.name().firstName(),
                faker.name().lastName()
        );

        Usuario userTest = new Usuario(
                25L,
                name,
                username,
                "1234",
                new ArrayList<>()
        );

        var content = new JSONObject();

        content.put("id", userTest.getId());
        content.put("name", userTest.getName());
        content.put("username", userTest.getUsername());
        content.put("password", userTest.getPassword());

        log.info(content.toString());

        String tokenOwner = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9." +
                "eyJzdWIiOiJDUjciLCJyb2xlcyI6WyJST0xFX09XTkVSIl0sImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9hcGkvbG9naW4iLCJleHAiOjE2MzM5NTA3Njl9." +
                "B7XDRf-yNwOhYFKsia8UcbaVCtTwSrlG_D23B5behEA";


        mockMvc.perform(post("/api/users/save")
                        .header("Authorization", tokenOwner)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content.toString()));


        MvcResult getStudentsResult = mockMvc.perform(get("/api/users")
                        .header("Authorization", tokenOwner)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String contentAsString = getStudentsResult
                .getResponse()
                .getContentAsString();

        List<Usuario> users = objectMapper.readValue(
                contentAsString,
                new TypeReference<>() {
                }
        );

        long id = users.stream()
                .filter(s -> s.getUsername().equals(userTest.getUsername()))
                .map(Usuario::getId)
                .findFirst()
                .orElseThrow(() ->
                        new IllegalStateException(
                                "student with username: " + username + " not found"));

        // when
        ResultActions resultActions = mockMvc
                .perform(delete("/api/users/delete/" + id)
                        .header("Authorization", tokenOwner));

        // then
        resultActions.andExpect(status().isOk());
        boolean exists = userRepository.existsById(id);
        assertThat(exists).isFalse();
    }


}