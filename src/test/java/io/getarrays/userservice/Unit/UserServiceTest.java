package io.getarrays.userservice.Unit;

import io.getarrays.userservice.model.Role;
import io.getarrays.userservice.model.Usuario;
import io.getarrays.userservice.repository.RoleRepository;
import io.getarrays.userservice.repository.UserRepository;
import io.getarrays.userservice.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private PasswordEncoder passwordEncoder;

    private UserServiceImpl userServiceTest;

    @BeforeEach
    void setUp() {
        userServiceTest = new UserServiceImpl(userRepository, roleRepository, passwordEncoder);
    }

    @Test
    void canSaveAnUser() {
        // given
        Usuario userTest = new Usuario(
                1L,
                "Marcus",
                "ney",
                "1234",
                new ArrayList<>()
        );

        // when
        userServiceTest.saveUser(userTest);

        // then
        ArgumentCaptor<Usuario> userArgumentCaptor =
                ArgumentCaptor.forClass(Usuario.class);

        verify(userRepository)
                .save(userArgumentCaptor.capture());

        Usuario capturedUser = userArgumentCaptor.getValue();

        assertThat(capturedUser).isEqualTo(userTest);
    }

    @Test
    void canSaveAnRole() {
        // given
        Role roleTest = new Role(
                1L,
                "ROLE_USER"
        );

        // when
        userServiceTest.saveRole(roleTest);

        // then
        ArgumentCaptor<Role> roleArgumentCaptor =
                ArgumentCaptor.forClass(Role.class);

        verify(roleRepository)
                .save(roleArgumentCaptor.capture());

        Role capturedRole = roleArgumentCaptor.getValue();

        assertThat(capturedRole).isEqualTo(roleTest);
    }

    @Test
    void canAddRoleToUser() {
        // given
        Role roleTest = new Role(
                1L,
                "ROLE_USER"
        );

        Usuario userTest = new Usuario(
                2L,
                "Marcus",
                "marcus",
                "1234",
                new ArrayList<>()
        );

        // when
        userServiceTest.saveRole(roleTest);
        userServiceTest.saveUser(userTest);

        userTest.getRoles().add(roleTest);

        // then
        Collection<Role> capturedRoles = userTest.getRoles();

        assertThat(roleTest).isIn(capturedRoles);
    }

    @Test
    void canGetAnUsersByUsername() {
        // given
        String username = "marcus";

        // when
        userServiceTest.getUser(username);

        // then
        verify(userRepository).findByUsername(username);
    }

    @Test
    void canGetAllUsers() {
        // when
        userServiceTest.getUsers();
        // then
        verify(userRepository).findAll();
    }

    @Test
    void deleteUser() {
        // given
        long id = 10;
        given(userRepository.existsById(id))
                .willReturn(true);
        // when
        userServiceTest.deleteUser(id);

        // then
        verify(userRepository).deleteById(id);
    }

    @Test
    void willThrowWhenDeleteUserNotFound() {
        // given
        long id = 10;
        given(userRepository.existsById(id))
                .willReturn(false);
        // when
        // then
        assertThatThrownBy(() -> userServiceTest.deleteUser(id))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("User did not exist in the database");

        verify(userRepository, never()).deleteById(any());
    }

}