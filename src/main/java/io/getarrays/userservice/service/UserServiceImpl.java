package io.getarrays.userservice.service;

import io.getarrays.userservice.model.Role;
import io.getarrays.userservice.model.Usuario;
import io.getarrays.userservice.repository.RoleRepository;
import io.getarrays.userservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario user = userRepository.findByUsername(username);
        if(user == null){
            String message = "USER not found in the data base";
            log.error(message);
            throw new UsernameNotFoundException(message);
        }
        else{
            log.info("User found in the data base : {}", username);
        }

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });


        return new User(user.getUsername(),user.getPassword(),authorities);
    }

    @Override
    public Usuario saveUser(Usuario user) {
        log.info("saving new user in the database");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public Role saveRole(Role role) {
        log.info("saving new role {} in the database", role.getName());
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        log.info("Adding role {} to te user {}", roleName, username);
        Usuario user = userRepository.findByUsername(username);
        Role role = roleRepository.findRoleByName(roleName);

        user.getRoles().add(role);

    }

    @Override
    public Usuario getUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<Usuario> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<Usuario> deleteUser(Long userId) {

        boolean exists = userRepository.existsById(userId);
        Optional<Usuario> userDeleted;
        if(!exists){
            throw new IllegalStateException("User did not exist in the database");
        }
        else {
            userDeleted = userRepository.findById(userId);
            log.info("deleting user to the database");
            userRepository.deleteById(userId);
        }

        return userDeleted;
    }

    @Override
    @Transactional
    public void updateUser(Long userId, String username, String password) {
        Usuario user = userRepository.findById(userId)
                .orElseThrow(() -> new IllegalStateException("user with id"+ userId + "does not exists"));

        if(username != null && username.length() > 0 && !Objects.equals(user.getUsername(), username)){
            log.info("changing the username :{} to {}",user.getUsername(), username);
            user.setUsername(username);
        }

        if(password != null && password.length() > 0){
            log.info("changing the password :{} to {}",user.getPassword(), passwordEncoder.encode(password));
            user.setPassword(passwordEncoder.encode(password));
        }
    }

}
