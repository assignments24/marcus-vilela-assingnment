package io.getarrays.userservice.service;

import io.getarrays.userservice.model.Role;
import io.getarrays.userservice.model.Usuario;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Usuario saveUser(Usuario user);
    Role saveRole(Role role);

    void addRoleToUser(String username, String roleName);
    Usuario getUser(String username);
    List<Usuario> getUsers();

    Optional<Usuario> deleteUser(Long userId);

    void updateUser(Long userId, String username, String password);
}
