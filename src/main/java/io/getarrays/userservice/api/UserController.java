package io.getarrays.userservice.api;

import io.getarrays.userservice.model.Role;
import io.getarrays.userservice.model.Usuario;
import io.getarrays.userservice.service.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RequestMapping(path = "/api")
@RestController
public class UserController {

    private final UserService userService;

    @GetMapping(path = "/users")
    public ResponseEntity<List<Usuario>>getUsers(){
        return ResponseEntity.ok().body(userService.getUsers());
    }

    @PostMapping(path = "/users/save")
    public ResponseEntity<Usuario>saveUser(@RequestBody Usuario usuario){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/save").toUriString());
        return ResponseEntity.created(uri).body(userService.saveUser(usuario));
    }

    @PostMapping(path = "/role/save")
    public ResponseEntity<Role>saveRole (@RequestBody Role role){
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/save").toUriString());
        return ResponseEntity.created(uri).body(userService.saveRole(role));
    }

    @PostMapping(path = "/role/addToUser")
    public ResponseEntity<?>addRoleToUser (@RequestBody RoleToUserForm form){
        userService.addRoleToUser(form.getUsername(), form.getRoleName());
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/users/update/{userId}")
    public ResponseEntity<Usuario> updateUser(@PathVariable("userId") Long userId,
                                        @RequestParam(required = false) String username,
                                        @RequestParam(required = false) String password
                                        ) {
        userService.updateUser(userId, username, password);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/users/delete/{userId}")
    public Optional<Usuario> deleteUser(@PathVariable("userId") Long userId) {
        return userService.deleteUser(userId);
    }


}


@Data
class RoleToUserForm {
    private String username;
    private String roleName;

}