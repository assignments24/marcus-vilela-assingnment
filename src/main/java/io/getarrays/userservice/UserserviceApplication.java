package io.getarrays.userservice;

import io.getarrays.userservice.model.Role;
import io.getarrays.userservice.model.Usuario;
import io.getarrays.userservice.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@SpringBootApplication
public class  UserserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserserviceApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}


	@Bean
	CommandLineRunner commandLineRunner(UserService userService){
		return  args -> {
			userService.saveRole(new Role(null,"ROLE_USER"));
			userService.saveRole(new Role(null,"ROLE_MANAGER"));
			userService.saveRole(new Role(null,"ROLE_ADMIN"));
			userService.saveRole(new Role(null,"ROLE_OWNER"));

			userService.saveUser(new Usuario(null,"Will Smith", "princeofbellair", "1234", new ArrayList<>()));
			userService.saveUser(new Usuario(null,"Cristiano Ronaldo", "CR7", "omilhor", new ArrayList<>()));
			userService.saveUser(new Usuario(null,"NeymarJR", "ney", "menino", new ArrayList<>()));

			userService.addRoleToUser("CR7","ROLE_OWNER");
			userService.addRoleToUser("ney","ROLE_USER");
			userService.addRoleToUser("princeofbellair","ROLE_MANAGER");

		};
	}

}
