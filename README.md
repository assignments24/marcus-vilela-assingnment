# marcus-vilela-assigment

Was created a small CRUD applications using some Spring technologies.

## Project Structure

![1](assignment-structure.png)

## Project Instruction

In this project, to test the endpoints, 
a postman export was made available ( SECURITY.postman_collection.json ),
so just import the file in postman that the environment for testing is already configured.
