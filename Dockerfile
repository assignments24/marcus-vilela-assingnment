FROM openjdk:11.0.12-slim
WORKDIR /app

COPY .mvn ./.mvn
COPY mvnw pom.xml ./
COPY src ./src

EXPOSE 8080

CMD ["./mvnw" , "spring-boot:run"]

